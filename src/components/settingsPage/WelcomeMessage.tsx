import * as React from 'react'
import { AppConsumer } from '../../app/AppContext'

const WelcomeMessage = (props = { appName: '' }) => {
  return (
    <AppConsumer>
      {({ firstVisit }) =>
        firstVisit ? (
          <div>
            <h1>Welcome to {props.appName}</h1>
            <div>Please select your favorite coins to begin. </div>
          </div>
        ) : (
          ''
        )
      }
    </AppConsumer>
  )
}

export default WelcomeMessage
