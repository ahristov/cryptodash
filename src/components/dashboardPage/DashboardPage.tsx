import * as React from 'react'
import Page from '../../shared/Page'
import PriceGrid from './PriceGrid'

const DashboardPage: React.FC<{}> = () => {
  return (
    <Page name="dashboard">
      <PriceGrid />
    </Page>
  )
}

export default DashboardPage
