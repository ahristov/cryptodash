import { ICoinList } from 'cryptocompare'
import * as React from 'react'
import styled from 'styled-components'
import { AppConsumer, FilteredCoinsType } from '../../app/AppContext'
import CoinTile from './CoinTile'

const CoinGridStyled = styled.div`
  display: grid;
  /* grid-template-columns: repeat(5, 1fr); */
  grid-template-columns: repeat(auto-fill, minmax(130px, 1fr));
  grid-gap: 15px;
  margin-top: 40px;
`

const getLowerSectionCoins = (coinList: ICoinList, filteredCoins?: FilteredCoinsType): string[] => {
  console.log('filteredCoins', filteredCoins)
  return ((filteredCoins && Object.keys(filteredCoins)) || Object.keys(coinList)).slice(0, 50)
}

const getCoinsToDisplay = (
  coinList: ICoinList,
  favorites: string[],
  filteredCoins?: FilteredCoinsType,
  topSection?: boolean
): string[] => (topSection ? favorites : getLowerSectionCoins(coinList, filteredCoins))

interface ICoinGridProps {
  topSection?: boolean
}

const CoinGrid: React.FunctionComponent<ICoinGridProps> = ({ topSection }) => {
  return (
    <AppConsumer>
      {({ coinList, favorites, filteredCoins }) => (
        <CoinGridStyled>
          {coinList &&
            getCoinsToDisplay(coinList, favorites, filteredCoins, topSection).map(coinKey => (
              <CoinTile key={coinKey} coinKey={coinKey} topSection={topSection} />
            ))}
        </CoinGridStyled>
      )}
    </AppConsumer>
  )
}

export default CoinGrid
