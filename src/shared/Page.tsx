import * as React from 'react'
import { AppConsumer } from '../app/AppContext'

interface IPageProps {
  name: string
}

const Page: React.FunctionComponent<IPageProps> = ({ name, children }) => (
  <AppConsumer>
    {({ page }) => {
      if (page !== name) {
        return ''
      }
      return <div>{children}</div>
    }}
  </AppConsumer>
)

export default Page
