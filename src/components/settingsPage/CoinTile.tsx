import * as React from 'react'
import { AppConsumer } from '../../app/AppContext'
import { DeletableTile, DisabledTile, SelectableTile } from '../elements/Tile'
import CoinHeaderGrid from './CoinHeaderGrid'
import CoinImage from './CoinImage'

interface ICoinTileProps {
  coinKey: string
  topSection?: boolean
}

const clickCoinHandler = (
  coinKey: string,
  addCoin: (key: string) => void,
  removeCoin: (key: string) => void,
  topSection?: boolean
) => (topSection ? () => removeCoin(coinKey) : () => addCoin(coinKey))

const CoinTile: React.FunctionComponent<ICoinTileProps> = ({ coinKey, topSection }) => (
  <AppConsumer>
    {({ coinList, addCoin, removeCoin, isInFavorites }) => {
      const coin = coinList ? coinList[coinKey] : null

      let TileClass = SelectableTile

      if (topSection) {
        TileClass = DeletableTile
      } else if (isInFavorites(coinKey)) {
        TileClass = DisabledTile
      }

      return (
        coin && (
          <TileClass onClick={clickCoinHandler(coinKey, addCoin, removeCoin, topSection)}>
            <CoinHeaderGrid name={coin.CoinName} symbol={coin.Symbol} topSection={topSection} />
            <CoinImage symbol={coin.Symbol} imageUrl={coin.ImageUrl} />
          </TileClass>
        )
      )
    }}
  </AppConsumer>
)

export default CoinTile
