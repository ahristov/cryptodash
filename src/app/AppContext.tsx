import * as cc from 'cryptocompare'
import * as _ from 'lodash'
import * as React from 'react'

interface IAction {
  type: string
  payload: any
}

type DispatchType = (action: IAction) => void

interface IDictionary<T> {
  [index: string]: T
}

export type FilteredCoinsType = IDictionary<cc.ICoin>

interface IState {
  appName: string
  page: string
  dispatch: DispatchType
  firstVisit?: boolean
  displayCurrency: string
  coinList?: cc.ICoinList
  favorites: string[]
  currentFavorite?: string
  filteredCoins?: FilteredCoinsType
  setPage: (page: string) => void
  addCoin: (key: string) => void
  removeCoin: (key: string) => void
  prices?: cc.ICoinFullPrice[]
  isInFavorites: (key: string) => boolean
  confirmFavorites: () => void
  setFilteredCoins: (filteredCoins?: FilteredCoinsType | null) => void
  setCurrentFavorite: (sym: string) => void
}

const Context = React.createContext<IState>({} as IState)

const reducer = (state: IState, action: IAction) => {
  switch (action.type) {
    default:
      return state
  }
}

const LOCAL_STORAGE_ITEM_NAME = 'cryptodash'
const MAX_FAVORITES = 10

class AppProvider extends React.Component<{}, IState> {
  // readonly localStorageItemName = 'cryptodash'

  constructor(props: {}) {
    super(props)

    this.state = {
      addCoin: this.addCoin,
      appName: 'CryptoDash',
      confirmFavorites: this.confirmFavorites,
      dispatch: (action: IAction) => {
        this.setState(state => reducer(state as IState, action))
      },
      displayCurrency: 'USD',
      favorites: ['BTC', 'ETH', 'XMR', 'DOGE'],
      isInFavorites: this.isInFavorites,
      page: 'dashboard',
      removeCoin: this.removeCoin,
      ...this.savedSettings(),
      setCurrentFavorite: this.setCurrentFavorite,
      setFilteredCoins: this.setFilteredCoins,
      setPage: this.setPage
    }
  }

  componentDidMount = () => {
    this.fetchCoins()
    this.fetchPrices()
  }

  fetchCoins = async () => {
    const coinList = (await cc.coinList()).Data
    this.setState({ coinList })
  }

  fetchPrices = async () => {
    if (this.state.firstVisit) {
      return
    }
    const prices = await this.prices()
    this.setState({ prices })
  }

  prices = async () => {
    const returnData = []
    for (let i = 0, j = this.state.favorites.length; i < j; i++) {
      try {
        const sym = this.state.favorites[i]
        const priceData = await cc.priceFull(sym, this.state.displayCurrency, null)
        returnData.push(priceData)
      } catch (e) {
        console.warn('Fetch price data:', e)
      }
    }
    return returnData
  }

  addCoin = (key: string) => {
    const favorites = [...this.state.favorites]
    if (favorites.length < MAX_FAVORITES) {
      favorites.push(key)
      this.setState({ favorites })
    }
  }

  removeCoin = (key: string) => {
    const favorites = [...this.state.favorites]
    this.setState({ favorites: _.pull(favorites, key) })
  }

  isInFavorites = (key: string) => _.includes(this.state.favorites, key)

  savedSettings = () => {
    const storedString = localStorage.getItem(LOCAL_STORAGE_ITEM_NAME)
    const savedData = storedString ? JSON.parse(storedString) : null
    if (!savedData) {
      return {
        firstVisit: true,
        page: 'settings'
      }
    }
    const { currentFavorite, favorites } = savedData
    return { currentFavorite, favorites }
  }

  confirmFavorites = () => {
    const currentFavorite = this.state.favorites && this.state.favorites[0]
    this.setState(
      {
        currentFavorite,
        firstVisit: false,
        page: 'dashboard'
      },
      () => {
        this.fetchPrices()
      }
    )
    localStorage.setItem(
      LOCAL_STORAGE_ITEM_NAME,
      JSON.stringify({
        currentFavorite,
        favorites: this.state.favorites
      })
    )
  }

  setCurrentFavorite = (sym: string) => {
    this.setState({ currentFavorite: sym })
    localStorage.setItem(
      LOCAL_STORAGE_ITEM_NAME,
      JSON.stringify({
        ...JSON.parse(localStorage.getItem(LOCAL_STORAGE_ITEM_NAME) || '{}'),
        currentFavorite: sym
      })
    )
  }

  setPage = (page: string) => this.setState({ page })

  setFilteredCoins = (filteredCoins: FilteredCoinsType) => this.setState({ filteredCoins })

  render = () => <Context.Provider value={this.state}>{this.props.children}</Context.Provider>
}

const AppConsumer = Context.Consumer

export { AppConsumer, AppProvider }
