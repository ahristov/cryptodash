import styled from 'styled-components'

export const CenteredDiv = styled.div`
  display: grid;
  justify-content: center;
`
