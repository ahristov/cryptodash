import * as cc from 'cryptocompare'
import * as Fuse from 'fuse.js'
import * as _ from 'lodash'
import * as React from 'react'
import styled from 'styled-components'
import { AppConsumer, FilteredCoinsType } from '../../app/AppContext'
import { backgroundColor2, fontSize2 } from '../../shared/Styles'

const SearchGrid = styled.div`
  display: grid;
  grid-template-columns: 200px 1fr;
`

const SearchInput = styled.input`
  ${backgroundColor2}
  ${fontSize2}
  color: #1163c9;
  border: 1px solid;
  height: 25px;
  /* align-self: center;
  justify-self: left; */
  place-self: center left;
`

const fuseOptions = {
  distance: 1000,
  location: 0,
  maxPatternLength: 8,
  minMatchCharLength: 1,
  shouldSort: true,
  threshold: 0.2
}

const applyFilter = _.debounce(
  (inputValue: string, setFilteredCoins: (filteredCoins: FilteredCoinsType) => void, coinList: cc.ICoinList) => {
    // Get all the coin symbols
    const coinSymbols = Object.keys(coinList)
    // Get all the coin names, map symbol to name
    const coinNames = coinSymbols.map(sym => coinList[sym].CoinName)
    // Concatenate coin symbols and coin names together
    const allStringsToSearch = coinSymbols.concat(coinNames)
    // Initialize Fuse.js instance
    const fuzzy = new Fuse(allStringsToSearch, fuseOptions)
    // Fuzzy-search over the list of coin symbols and coin names
    // Returns the indexes of matched elements ordered by weight: [0, 1, 2, ..]
    const fuzzyIndexes = fuzzy.search(inputValue) as string[]
    // Get list of coin symbols and coin names that match the search.
    const fuzzyResults = _.map(fuzzyIndexes, idx => allStringsToSearch[idx])
    // Pick the coins with either equal coin symbol or coin name
    const filteredCoins = _.pickBy(coinList, (coin, symbol) => {
      const coinName = coin.CoinName
      return _.includes(fuzzyResults, symbol) || _.includes(fuzzyResults, coinName)
    })
    setFilteredCoins(filteredCoins)
  },
  500
)

const Search: React.FC = () => {
  return (
    <AppConsumer>
      {({ setFilteredCoins, coinList }) => {
        const handleOnKeyUp = (e: React.KeyboardEvent<HTMLInputElement>) => {
          const inputValue = e.currentTarget.value
          if (!inputValue) {
            setFilteredCoins(null)
            return
          }
          applyFilter(inputValue, setFilteredCoins, coinList || {})
        }
        return (
          <SearchGrid>
            <h2>Search all coins</h2>
            <SearchInput onKeyUp={handleOnKeyUp} />
          </SearchGrid>
        )
      }}
    </AppConsumer>
  )
}

export default Search
