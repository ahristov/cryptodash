declare module 'cryptocompare' {
  interface ICoin {
    Algorithm: string // "SHA-256"
    BlockNumber: number // 583843
    BlockReward: number // 12.5
    BlockTime: number // 600
    BuiltOn: string // "N/A"
    CoinName: string // "Bitcoin"
    FullName: string // "Bitcoin (BTC)"
    FullyPremined: string // "0"
    Id: string // "1182"
    ImageUrl: string // "/media/19633/btc.png"
    IsTrading: boolean // true
    Name: string // "BTC"
    NetHashesPerSecond: number // 56799756331.25
    PreMinedValue: string // "N/A"
    ProofType: string // "PoW"
    SmartContractAddress: string // "N/A"
    SortOrder: string // "1"
    Sponsored: boolean // false
    Symbol: string // "BTC"
    TotalCoinSupply: string // "21000000"
    TotalCoinsFreeFloat: string // "N/A"
    TotalCoinsMined: number // 17798050
    Url: string // "/coins/btc/overview"
  }

  interface ICoinList {
    [id: string]: ICoin
  }

  interface ICoinListResult {
    Data: ICoinList
  }

  interface IFullPrice {
    CHANGE24HOUR: number // 421.3000000000011
    CHANGEDAY: number // 600.4700000000012
    CHANGEPCT24HOUR: number // 3.7709064675566095
    CHANGEPCTDAY: number // 5.462189842639241
    FLAGS: string // "4"
    FROMSYMBOL: string // "BTC"
    HIGH24HOUR: number // 11720.72
    HIGHDAY: number // 11709.27
    HIGHHOUR: number // 11709.27
    IMAGEURL: string // "/media/19633/btc.png"
    LASTMARKET: string // "Bitfinex"
    LASTTRADEID: string // "375356176"
    LASTUPDATE: number // 1562430153
    LASTVOLUME: number // 0.0016
    LASTVOLUMETO: number // 18.5648
    LOW24HOUR: number // 10812.16
    LOWDAY: number // 10985.4
    LOWHOUR: number // 11578.58
    MARKET: string // "CCCAGG"
    MKTCAP: number // 206395178114.16
    OPEN24HOUR: number // 11172.38
    OPENDAY: number // 10993.21
    OPENHOUR: number // 11634.13
    PRICE: number // 11593.68
    SUPPLY: number // 17802387
    TOPTIERVOLUME24HOUR: number // 55090.78698363945
    TOPTIERVOLUME24HOURTO: number // 622185152.4839317
    TOSYMBOL: string // "USD"
    TOTALTOPTIERVOLUME24H: number // 231397.1161147653
    TOTALTOPTIERVOLUME24HTO: number // 2666224314.4048824
    TOTALVOLUME24H: number // 422554.6355126699
    TOTALVOLUME24HTO: number // 4881839714.150114
    TYPE: string // "5"
    VOLUME24HOUR: number // 57620.30397985646
    VOLUME24HOURTO: number // 650907853.3447652
    VOLUMEDAY: number // 34698.749580598
    VOLUMEDAYTO: number // 396858165.1979433
    VOLUMEHOUR: number // 1197.1587573031377
    VOLUMEHOURTO: number // 13951775.99807841
  }

  interface IFullPrices {
    [id: string]: IFullPrice // Full price by currency (key is USD, EUR, etc.)
  }

  export interface ICoinFullPrice {
    [id: string]: IFullPrices // List of full prices my coin symbol (BTC, etc.)
  }

  export function coinList(): ICoinListResult

  export function constituentExchangeList(options: any): any

  export function exchangeList(): any

  export function generateAvg(fsym: any, tsym: any, e: any, tryConversion: any): any

  export function histoDay(fsym: any, tsym: any, options: any): any

  export function histoHour(fsym: any, tsym: any, options: any): any

  export function histoMinute(fsym: any, tsym: any, options: any): any

  export function histoSocial(timePeriod: any, options: any): any

  export function latestSocial(options: any): any

  export function newsFeedsAndCategories(): any

  export function newsList(lang: any, options: any): any

  export function price(fsym: any, tsyms: any, options: any): any

  export function priceFull(fsyms: string, tsyms: string, options: any): ICoinFullPrice

  export function priceHistorical(fsym: any, tsyms: any, time: any, options: any): any

  export function priceMulti(fsyms: any, tsyms: any, options: any): any

  export function setApiKey(userApiKey: any): void

  export function topExchanges(fsym: any, tsym: any, limit: any): any

  export function topExchangesFull(fsym: any, tsym: any, limit: any): any

  export function topPairs(fsym: any, limit: any): any
}
