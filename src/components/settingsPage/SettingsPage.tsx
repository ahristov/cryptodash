import * as React from 'react'
import { AppConsumer } from '../../app/AppContext'
import Page from '../../shared/Page'
import CoinGrid from './CoinGrid'
import ConfirmSettingsButton from './ConfirmSettingsButton'
import Search from './Search'
import WelcomeMessage from './WelcomeMessage'

const SettingsPage = () => {
  return (
    <AppConsumer>
      {({ appName }) => (
        <Page name="settings">
          <WelcomeMessage appName={appName} />
          <CoinGrid topSection={true} />
          <ConfirmSettingsButton />
          <Search />
          <CoinGrid />
        </Page>
      )}
    </AppConsumer>
  )
}

export default SettingsPage
