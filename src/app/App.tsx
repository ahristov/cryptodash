import * as React from 'react'
import DashboardPage from '../components/dashboardPage'
import SettingsPage from '../components/settingsPage'
import LoadingContentWrapper from '../shared/LoadingContentWrapper'
import './App.css'
import AppBar from './AppBar'
import { AppProvider } from './AppContext'
import AppLayout from './AppLayout'

class App extends React.Component {
  render() {
    return (
      <AppLayout>
        <AppProvider>
          <AppBar />
          <LoadingContentWrapper>
            <SettingsPage />
            <DashboardPage />
          </LoadingContentWrapper>
        </AppProvider>
      </AppLayout>
    )
  }
}

export default App
