import styled, { css } from 'styled-components'

interface IButtonProps {
  readonly active?: boolean
}

const Button = styled.button<IButtonProps>`
  outline: none !important;
  cursor: pointer;
  display: inline-block;
  padding: 0.5rem 0;
  margin: 0.5rem 1rem;
  min-width: 10rem;
  /* background: transparent; */
  background-color: #441199;
  color: #0080ff;
  border: 2px solid #0080ff;
  &:hover {
    border-color: #80bfff;
    box-shadow: -3px 3px 15px #6633bb;
  }
  &:active {
    box-shadow: -3px 3px 15px #68da9f;
  }
  ${p =>
    p.active &&
    css`
      color: palevioletred;
      background-color: #5522aa;
    `}
`

const RoundedButton = styled(Button)`
  border-radius: 10px;
`

export { Button, RoundedButton }
