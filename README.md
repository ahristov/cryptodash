# Cryptocurrency Dashboard

This project contains code from studying [React Data Visualization](https://www.udemy.com/react-data-visualization-build-a-cryptocurrency-dashboard) course.

## 🏠 [Homepage](https://bitbucket.org/ahristov/cryptodash/)

## Install

```sh
yarn install
```

, or just `yarn`.

## Usage

```sh
yarn start
```

## Run tests

```sh
yarn test
```

## Author

👤 **Atanas Hristov**

## Course notes

### Links

[Styled components: Basics](https://www.styled-components.com/docs/basics)  
[Styled components: Custom props](https://www.styled-components.com/docs/api#typescript)  
[Google font used](https://fonts.google.com/specimen/Do+Hyeon?selection.family=Do+Hyeon)  
[React Context](https://reactjs.org/docs/context.html)  
[Crypto Compare: API](https://min-api.cryptocompare.com/)  
[Crypto Compare: API docs](https://min-api.cryptocompare.com/documentation)  
[Crypto Compare: Npm package](https://www.npmjs.com/package/cryptocompare)  
[dts-gen: A TypeScript Definition File Generator](https://github.com/microsoft/dts-gen)
[Auto-Sizing Columns in CSS Grid](https://css-tricks.com/auto-sizing-columns-css-grid-auto-fill-vs-auto-fit/)
