import * as React from 'react'
import styled from 'styled-components'
import { AppConsumer } from '../../app/AppContext'
import PriceTile from './PriceTile'

const PriceGridStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-gap: 15px;
  margin-top: 40px;
`

const PriceGrid: React.FC<{}> = () => {
  return (
    <AppConsumer>
      {({ prices, displayCurrency }) =>
        prices ? (
          <PriceGridStyled>
            {prices.map((price, index) => (
              <PriceTile key={index} index={index} displayCurrency={displayCurrency} price={price} />
            ))}
          </PriceGridStyled>
        ) : (
          ''
        )
      }
    </AppConsumer>
  )
}

export default PriceGrid
