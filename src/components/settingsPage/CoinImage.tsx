import * as React from 'react'

interface ICoinImageProps {
  symbol: string
  imageUrl: string
  style?: any
}

const CoinImage: React.FunctionComponent<ICoinImageProps> = ({ symbol, imageUrl, style }) => (
  <img alt={symbol} style={style || { height: '50px' }} src={`http://cryptocompare.com/${imageUrl}`} />
)

export default CoinImage
