import * as React from 'react'
import { AppConsumer } from '../app/AppContext'

const LoadingContentWrapper: React.FC<{}> = ({ children }) => {
  return (
    <AppConsumer>
      {({ coinList, prices, firstVisit }) => {
        if (!coinList) {
          return <div>Loading Coins ...</div>
        }
        if (!firstVisit && !prices) {
          return <div>Loading Prices ...</div>
        }
        return <div>{children}</div>
      }}
    </AppConsumer>
  )
}

export default LoadingContentWrapper
