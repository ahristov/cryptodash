import * as cc from 'cryptocompare'
import * as React from 'react'
import styled, { css } from 'styled-components'
import { AppConsumer } from '../../app/AppContext'
import { fontSize1, fontSize3, greenBoxShadow } from '../../shared/Styles'
import { SelectableTile } from '../elements/Tile'

const PriceTileStyled = styled(SelectableTile)<{ compact?: boolean; favorite?: boolean }>`
  ${props =>
    props.compact &&
    css`
      ${fontSize3}
      display: grid;
      grid-template-columns: repeat(3, 1fr);
      grid-gap: 5px;
      justify-items: right;
    `}
  ${props =>
    props.favorite &&
    css`
      ${greenBoxShadow}
      pointer-events: none;
    `}
`

const CoinHeaderGridStyled = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

const JustifiedLeft = styled.div`
  justify-self: left;
`

const JustifiedRight = styled.div`
  justify-self: right;
`

const TickerPrice = styled.div`
  ${fontSize1}
`

const ChangePercentStyled = styled.div<{ decreased: boolean }>`
  color: green;
  ${props =>
    props.decreased &&
    css`
      color: red;
    `}
`

const numberFormat = (num: number) => +(num + '').slice(0, 7)

const ChangePercentElement: React.FC<{ pct: number }> = ({ pct }) => {
  return (
    <JustifiedRight>
      <ChangePercentStyled decreased={pct < 0}>{numberFormat(pct)}</ChangePercentStyled>
    </JustifiedRight>
  )
}

interface IPriceTileElementProps {
  sym: string
  price: cc.IFullPrice
  displayCurrency: string
  favorite: boolean
  onClickHandler: () => void
}

const PriceTileElement: React.FC<IPriceTileElementProps> = ({
  sym,
  price,
  displayCurrency,
  favorite,
  onClickHandler
}) => {
  return (
    <PriceTileStyled onClick={onClickHandler} favorite={favorite}>
      <CoinHeaderGridStyled>
        <div> {sym} </div>
        <ChangePercentElement pct={price.CHANGEPCT24HOUR} />
      </CoinHeaderGridStyled>
      <TickerPrice>
        {numberFormat(price.PRICE)} {displayCurrency}
      </TickerPrice>
    </PriceTileStyled>
  )
}

const PriceTileElementCompact: React.FC<IPriceTileElementProps> = ({
  sym,
  price,
  displayCurrency,
  favorite,
  onClickHandler
}) => {
  return (
    <PriceTileStyled onClick={onClickHandler} compact={true} favorite={favorite}>
      <JustifiedLeft> {sym} </JustifiedLeft>
      <ChangePercentElement pct={price.CHANGEPCT24HOUR} />
      <div>
        {numberFormat(price.PRICE)} {displayCurrency}
      </div>
    </PriceTileStyled>
  )
}

interface IPriceTileProps {
  displayCurrency: string
  index: number
  price: cc.ICoinFullPrice
}

const PriceTile: React.FC<IPriceTileProps> = ({ displayCurrency, index, price }) => {
  const sym = Object.keys(price)[0]
  if (sym) {
    const TileClass = index < 5 ? PriceTileElement : PriceTileElementCompact
    const currentPrice = price[sym][displayCurrency]
    return (
      <AppConsumer>
        {({ currentFavorite, setCurrentFavorite }) => {
          const onClickHandler = () => setCurrentFavorite(sym)
          return (
            <TileClass
              sym={sym}
              price={currentPrice}
              displayCurrency={displayCurrency}
              favorite={currentFavorite === sym}
              onClickHandler={onClickHandler}
            />
          )
        }}
      </AppConsumer>
    )
  } else {
    return <React.Fragment />
  }
}

export default PriceTile
