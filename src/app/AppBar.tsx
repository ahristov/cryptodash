import * as React from 'react'
import styled from 'styled-components'
import { Button } from '../components/elements'
import { AppConsumer } from './AppContext'

const Logo = styled.div`
  margin-top: 0.5rem;
  font-size: 1.75em;
`

const Bar = styled.div`
  display: grid;
  margin-bottom: 40px;
  grid-template-columns: 180px auto 11rem 11rem;
`

const toProperCase = (s: string) => s.charAt(0).toUpperCase() + s.substr(1).toLowerCase()

interface IBarButtonProps {
  readonly active?: boolean
  readonly caption: string
}
const BarButton: React.FunctionComponent<IBarButtonProps> = props => {
  const { caption } = props
  return (
    <AppConsumer>
      {({ page, setPage }) => {
        const handleClick = () => setPage(caption)
        return (
          <Button active={caption === page} onClick={handleClick}>
            {toProperCase(caption)}
          </Button>
        )
      }}
    </AppConsumer>
  )
}

const AppBar = () => {
  return (
    <Bar>
      <Logo> CryptoDash </Logo>
      <div />
      <BarButton caption="dashboard" />
      <BarButton caption="settings" />
    </Bar>
  )
}

export default AppBar
