import * as React from 'react'
import { AppConsumer } from '../../app/AppContext'
import { CenteredDiv, RoundedButton } from '../elements'

const ConfirmSettingsButton = () => {
  return (
    <AppConsumer>
      {({ confirmFavorites }) => (
        <CenteredDiv>
          <RoundedButton onClick={confirmFavorites}>Confirm Favorites</RoundedButton>
        </CenteredDiv>
      )}
    </AppConsumer>
  )
}

export default ConfirmSettingsButton
